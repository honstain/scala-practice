## A basic repo for learning and experimenting with Scala

Run tests
```
sbt test
```

Build and run from command line using sbt
```
sbt
~run
```