import org.scalatest.FlatSpec
import org.scalatest.Matchers._


class HelloWorldTest extends FlatSpec {

  "The upper case function" should "handle empty Array input" in {
    val result = HelloWorld.toUpperTest(Array())
    result should contain theSameElementsInOrderAs (Array[String]())
  }

  "The upper case function" should "process Array with single element" in {
    // How to compare the array's https://stackoverflow.com/questions/5393243/how-do-i-compare-two-arrays-in-scala
    val result = HelloWorld.toUpperTest(Array("a"))
    result should contain theSameElementsInOrderAs (Array("A"))
  }

  "The upper case function" should "process Array with multiple elements" in {
    // How to compare the array's https://stackoverflow.com/questions/5393243/how-do-i-compare-two-arrays-in-scala
    val result = HelloWorld.toUpperTest(Array("Aa", "b"))
    result should contain theSameElementsInOrderAs(Array("AA", "B"))
  }

}