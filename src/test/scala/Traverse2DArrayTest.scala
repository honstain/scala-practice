import org.scalatest.FlatSpec
import org.scalatest.Matchers._


import scala.collection.mutable.ArrayBuffer

class Traverse2DArrayTest extends FlatSpec {

  "traverseCounterClockwise" should "traverse 1X1" in {
    val testArray = Array(
      Array(0),
    )
    val result = Traverse2DArray.traverseCounterClockwise(testArray)
    result should contain theSameElementsInOrderAs ArrayBuffer(
      (0,0),
    )
  }

  "traverseCounterClockwise" should "traverse 1X2" in {
    val testArray = Array(
      Array(0),
      Array(1),
    )
    val result = Traverse2DArray.traverseCounterClockwise(testArray)
    result should contain theSameElementsInOrderAs ArrayBuffer(
      (0,0),
      (0,1),
    )
  }

  "traverseCounterClockwise" should "traverse 2X1" in {
    val testArray = Array(
      Array(0, 1),
    )
    val result = Traverse2DArray.traverseCounterClockwise(testArray)
    result should contain theSameElementsInOrderAs ArrayBuffer(
      (0,0), (1,0),
    )
  }

  "traverseCounterClockwise" should "traverse 2X2" in {
    val testArray = Array(
      Array(0, 3),
      Array(1, 2),
    )
    val result = Traverse2DArray.traverseCounterClockwise(testArray)
    result should contain theSameElementsInOrderAs ArrayBuffer(
      (0,0), (0,1),
      (1,1), (1,0),
    )
  }

  "traverseCounterClockwise" should "traverse 3X3" in {
    val testArray = Array(
      Array(0, 7, 6),
      Array(1, 8, 5),
      Array(2, 3, 4),
    )
    val result = Traverse2DArray.traverseCounterClockwise(testArray)
    result should contain theSameElementsInOrderAs ArrayBuffer(
      (0,0), (0,1), (0, 2), (1,2), (2,2), (2,1), (2,0), (1,0), (1,1)
    )
  }

  "traverseCounterClockwise" should "traverse 4X4" in {
    val testArray = Array(
      Array(0, 11, 10, 9),
      Array(1, 12, 15, 8),
      Array(2, 13, 14, 7),
      Array(3, 4,  5,  6),
    )
    val result = Traverse2DArray.traverseCounterClockwise(testArray)
    result should contain theSameElementsInOrderAs ArrayBuffer(
      (0,0), (0,1), (0, 2), (0, 3),
      (1,3), (2,3), (3,3),
      (3,2), (3,1), (3,0),
      (2,0), (1,0),
      (1,1), (1,2),
      (2,2), (2,1),
    )
  }

}
