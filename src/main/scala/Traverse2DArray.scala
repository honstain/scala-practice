import scala.collection.mutable.ArrayBuffer


object Traverse2DArray {

  def traverseCounterClockwise(input: Array[Array[Int]]): ArrayBuffer[(Int, Int)] = {
    val result = new ArrayBuffer[(Int, Int)]()
    var min_x = 0
    var min_y = 0
    var max_x = input(0).length - 1
    var max_y = input.length - 1

    while (min_y <= max_y && min_x <= max_x) {
      for (y <- min_y to max_y) {
        result.append((min_x, y))
      }
      min_x += 1

      if (min_x <= max_x) {
        for (x <- min_x to max_x) {
          result.append((x, max_y))
        }
        max_y -= 1
      }

      if (min_y <= max_y && min_x <= max_x) {
        for (y <- max_y to min_y by -1) {
          result.append((max_x, y))
        }
        max_x -= 1
      }

      if (min_y <= max_y && min_x < max_x) {
        for (x <- max_x to min_x by -1) {
          result.append((x, min_y))
        }
        min_y += 1
      }
    }

    result
  }

}
