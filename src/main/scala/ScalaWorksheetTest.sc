for (x <- 2 to 0 by -1) println(x)

(2 to 0 by -1).toList

abstract class Tree
case class Sum(l: Tree, r: Tree) extends Tree
case class Var(n: String) extends Tree
case class Const(v: Int) extends Tree

type Environment = String => Int

def eval(t: Tree, env: Environment): Int = t match {
  case Sum(l, r) => eval(l, env) + eval(r, env)
  case Var(n)    => env(n)
  case Const(v)  => v
}

val exp: Tree = Sum(Var("x"), Const(7))
val env: Environment = { case "x" => 5 }
println("Expression:" + exp)
println("Eval:" + eval(exp, env))

trait Ord {
  def < (that: Any): Boolean
  def <= (that: Any): Boolean = (this < that) || (this == that)
  def > (that: Any): Boolean = !(this <= that)
  def >= (that: Any): Boolean = !(this < that)
}

class Date(y: Int, m: Int, d: Int) extends Ord {
  def year = y
  def month = m
  def day = d
  override def toString(): String = {
    year + "-" + month + "-" + day
  }

  override def equals(that: Any): Boolean = {
    this.isInstanceOf[Date] && {
      val o = that.asInstanceOf[Date]
      o.day == day && o.month == month && o.year == year
    }
  }

  def <(that: Any) : Boolean = {
    if (!that.isInstanceOf[Date])
      sys.error("cannot compare " + that + " and a Date")

    val o = that.asInstanceOf[Date]
    (year < o.year) ||
      (year == o.year && (month < o.month ||
        month == o.month && day < o.day))
  }
}

val date1 = new Date(2018, 8, 31)
val date2 = new Date(2018, 8, 30)

date1 < date2
date1 <= date2
date1 > date2
date1 >= date2

