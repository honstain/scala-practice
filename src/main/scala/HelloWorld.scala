object HelloWorld extends App {

  // ------------------------------------------------------------
  // Trying to refactor this logic out to its own function
  def toUpperTest(args: Array[String]): Array[String]  = {
    for (a <- args) yield a.toUpperCase
  }

  var res = toUpperTest(Array("Hello, world!", "Foo"))
  for (a <- res) {
    println(a)
  }
  // ------------------------------------------------------------
  println("Debug - doing it manually now")

  val arrayStrings = Array("Hello, world!", "Foo")
  val resManual = for (a <- arrayStrings) yield a.toUpperCase

  println(resManual)
  for (a <- resManual) {
    println(a)
  }
}
