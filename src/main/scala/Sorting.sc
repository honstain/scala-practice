

// Quicksort coding using a traditional style.
def sort(xs: Array[Int]): Unit = {
  def swap(i: Int, j: Int): Unit = {
    val t = xs(i)
    xs(i) = xs(j)
    xs(j) = t
  }

  def sort1(l: Int, r: Int): Unit = {
    for (x <- l until r + 1) {
      println("l", l, "r", r, "l", x, "value: ", xs(x))
    }

    val pivot = xs((l+r) / 2)
    var i = l
    var j = r

    while (i <= j) {
      while (xs(i) < pivot) i += 1
      while (xs(j) > pivot) j -= 1
      if (i <= j) {
        println("\tl", l, "r", r, "i", i, "j", j)
        swap(i, j)
        i += 1
        j -= 1
      }
    }

    for (x <- l until r + 1) {
      println(x, "value: ", xs(x))
    }
  }
  sort1(0, xs.length - 1)
}

sort(Array(1,2,3,4))

sort(Array(4,3,2,1))
