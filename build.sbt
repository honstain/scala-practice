name := "scala-practice"

version := "0.1"

scalaVersion := "2.12.6"

// addSbtPlugin("com.artima.supersafe" % "sbtplugin" % "1.1.3")

libraryDependencies ++= Seq(
  // http://www.scalatest.org/install
  "org.scalactic" %% "scalactic" % "3.0.5",
  // Note the "test" means this package will only be available in the test classpath
  "org.scalatest" %% "scalatest" % "3.0.5" % "test"
)